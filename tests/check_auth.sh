#!/bin/bash

res=`curl -sL -w "%{http_code}\\n" "http://localhost/auth" -o /dev/null`
if [ $res = 401 ]; then
   exit 0
else
   exit 1
fi
